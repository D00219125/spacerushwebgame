class PowerShot extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("PowerShot - " + objectManager.PowerShotCount,
            ActorType.Bullet,
            new Transform2D(
                BULLET_ALL_INITIAL_TRANSLATION, 
                0,
                new Vector2(1,1),
                Vector2.Zero,
                new Vector2(
                    BULLET_POWERSHOT_WIDTH,
                    BULLET_POWERSHOT_HEIGHT
                )
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                BULLET_POWERSHOT_ANIMATION_FPS,
                BULLET_POWERSHOT_CELLS,
                0
            ),       
            StatusType.Off
        );

        this.ctx = ctx;
        this.spriteSheet = spriteSheet;
        this.objectManager = objectManager;

        this.AttachBehavior(
            new MoveBehavior(BULLET_POWERSHOT_DIRECTION, BULLET_POWERSHOT_MOVE_SPEED)
        );
    }

    Clone() 
    {
        //make a clone of the actor
        let clone = new PowerShot(this.ctx, this.spriteSheet, this.objectManager);

        //now clone all the actors attached behaviors
        for(let behavior of this.behaviors)
            clone.AttachBehavior(behavior.Clone());
        
        //lastly return the actor
        return clone;
    }
}