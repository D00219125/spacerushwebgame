class SpreadShotPickup extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("spreadShotPickup - " + objectManager.SpreadShotCount,
            ActorType.Interactable,
            new Transform2D(
                new Vector2(
                Math.random() * (cvs.clientWidth - SPREADSHOT_PICKUP_CELLS_WIDTH), -SPREADSHOT_PICKUP_CELLS_HEIGHT
            ),
            0,
                new Vector2(1,1),
                new Vector2(0,0),
                new Vector2(SPREADSHOT_PICKUP_CELLS[0].width, SPREADSHOT_PICKUP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                SPREADSHOT_PICKUP_ANIMATION_FPS, 
                SPREADSHOT_PICKUP_CELLS,
                Math.floor(Math.random() * SPREADSHOT_PICKUP_CELLS.length)
            ),       
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, SPREADSHOT_PICKUP_BASE_FALL_SPEED),
            0
        );
        objectManager.Add(this);
    }
}