class BigShip extends Sprite
{
    
    constructor(ctx, SpriteSheet, objectManager, screenRectangle)
    {
        super(("BigShip - " + objectManager.BigShipCount),
            ActorType.Enemy,
            new Transform2D(
                new Vector2(
                    //Translate random X value of canvas width, and above the screen for Y
                    Math.random() * (cvs.clientWidth - BIG_SHIP_CELLS_WIDTH), -BIG_SHIP_CELLS_HEIGHT
                ),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(BIG_SHIP_CELLS[0].width, BIG_SHIP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                SpriteSheet,
                BIG_SHIP_ANIMATION_FPS,
                BIG_SHIP_CELLS,
                Math.floor(Math.random() * ASTEROID_CELLS.length) //Random start cells
            ),
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, BIG_SHIP_BASE_FALL_SPEED),
            0
        );

        this.AttachBehavior(
            new EnemyMoveBehavior(objectManager, screenRectangle, BIG_SHIP_HEIGHT_STOP)
        );

        this.AttachBehavior(
            new EnemyTrackFireBehavior(
                objectManager, new EnemyBullet(ctx, spriteSheet, objectManager),
                BULLET_ENEMY_FIRE_INTERVAL_IN_MS, this)
        );
        
        objectManager.Add(this);
    }
    
}