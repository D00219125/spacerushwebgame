class MidShip extends Sprite
{
    constructor(ctx, SpriteSheet, objectManager, screenRectangle)
    {
        super(("MidShip - " + objectManager.MidShipCount),
            ActorType.Enemy,
            new Transform2D(
                new Vector2(
                    //Translate random X value of canvas width, and above the screen for Y
                    Math.random() * (cvs.clientWidth - MID_SHIP_CELLS_WIDTH), -MID_SHIP_CELLS_HEIGHT
                ),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(MID_SHIP_CELLS[0].width, MID_SHIP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                SpriteSheet,
                MID_SHIP_ANIMATION_FPS,
                MID_SHIP_CELLS,
                Math.floor(Math.random() * ASTEROID_CELLS.length) //Random start cells
            ),
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, MID_SHIP_BASE_FALL_SPEED),
            0
        );

        this.AttachBehavior(
            new EnemyMoveBehavior(objectManager, screenRectangle, MID_SHIP_HEIGHT_STOP)
        );

        this.AttachBehavior(
            new EnemyFireBehavior(
                objectManager, new EnemyBullet(ctx, spriteSheet, objectManager),
                BULLET_ENEMY_FIRE_INTERVAL_IN_MS, this)
        );
        
        objectManager.Add(this);
    }
}