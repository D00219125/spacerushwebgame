
//#region Development Diary

/*
------
To Do:
- Add better spawning system
- Add enemy and player collision
- Add "Enemy" turrets
- Add turret tracking (player)
- Add enemy bullet and player collision
- Update all sprites
- Add Menu
- Add local Storage
- Add sound

Done:
- Added free player movement (up/down/left/right/diagonals)
- Added player shooting
- Added infinite scrolling background
- Added "Enemy" Asteroids
- Added enemy and bullet collision
- Added screen/canvas border collision
- Fixed(kind of) Collision borders out of place(scaling)
- Make asteroids spawn randomly
- Add score system




Maybe To Do After:
- Add - Upgrades/Collectables
- Add Shop?

*/

//#endregion

/********************************************************************* EVENT LISTENERS *********************************************************************/
//add event listener for load
window.addEventListener("load", LoadGame);

/********************************************************************* GLOBAL VARS *********************************************************************/
//get a handle to our canvas
var cvs = document.getElementById("game-canvas");
//get a handle to 3D context which allows drawing
var ctx = cvs.getContext("2d");

//stores elapsed time
var gameTime;
//assets
var spriteSheet, backgroundSpriteSheet, asteroidSpriteSheet;

//managers and notification
var objectManager;
var soundManager;
var notificationCenter;
var gameStateManager;
//...
var difficulty;
var fpsDropCount = 0;

//stores screen bounds in a rectangle
var screenRectangle;
//#endregion

/************************************************************ CORE GAME LOOP CODE UNDER THIS POINT ************************************************************/

// #region  LoadGame, Start, Animate 
function LoadGame() {
  //load content
  Initialize();

  //start timer - notice that it is called only after we loaded all the game content
  Start();
  StartNotifications();

}

function Start() {
  //runs in proportion to refresh rate
  document.getElementById("highscore").innerHTML = localStorage.getItem("Highscore") || 0;
  this.animationTimer = window.requestAnimationFrame(Animate);
  this.gameTime = new GameTime();
}

function StartNotifications()
{
  this.notificationCenter.Notify(new Notification(
    NotificationType.Time, NotificationAction.Start));
  this.notificationCenter.Notify(new Notification(
    NotificationType.Time, NotificationAction.Pause));

  this.notificationCenter.Notify(new Notification(
    NotificationType.Menu, NotificationAction.ShowMenuChanged,  [StatusType.Off]));
}

function Animate(now) {
  this.gameTime.Update(now);
  Update(this.gameTime);
  Draw(this.gameTime);
  window.requestAnimationFrame(Animate);
}

// #endregion 

// #region  Update, Draw 
function Update(gameTime) 
{
  if(gameTime.FPS < 30)
  {
    this.fpsDropCount++;
  }
  else this.fpsDropCount = 0;
  if (this.keyboardManager.IsKeyDown(Keys.P) || (fpsDropCount > 3))
  {
    this.notificationCenter.Notify(new Notification(NotificationType.Menu, 
        NotificationAction.ShowMenuChanged,  [StatusType.Off | StatusType.IsDrawn]));
    this.notificationCenter.Notify(new Notification(
      NotificationType.Time, NotificationAction.Pause));
      this.fpsDropCount = 0;
  }
  else if(this.keyboardManager.IsKeyDown(Keys.R) && this.objectManager.PlayerSprites.length > 0)
  {
    this.notificationCenter.Notify(new Notification(
      NotificationType.Menu, NotificationAction.ShowMenuChanged, [StatusType.IsUpdated | StatusType.IsDrawn]));
    this.notificationCenter.Notify(new Notification(
      NotificationType.Time, NotificationAction.Resume));
  }
  
  //update all the game sprites
  this.objectManager.Update(gameTime);

  //update game state
  this.gameStateManager.Update(gameTime);

  //updates the menu manager to listen for show/hide menu keystroke
  this.menuManager.Update(gameTime);

  //move everything down
  this.objectManager.DeltaTranslationOffset = new Vector2(0, BASE_GAME_FALL_SPEED);
  
  if(!this.gameStateManager.Timer.IsPaused)
  {
    difficulty = Math.round(this.gameStateManager.Timer.GetElapsedTime()/250);
    this.notificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Play,  ["sound_game"]));
    this.notificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Pause, ["sound_menu"]));

    if(this.gameStateManager.Timer.GetElapsedTime() % (2000 - difficulty) > (1980 - difficulty) 
    && this.asteroidSpawn)
    {
      SpawnAsteroids();
      this.asteroidSpawn = false;
    } else this.asteroidSpawn = true;

    if(this.gameStateManager.Timer.GetElapsedTime() % (3000 - difficulty) > (2980 - difficulty )
    && this.smallShipSpawn)
    {
      SpawnSmallShips();
      this.smallShipSpawn = false;
    }else this.smallShipSpawn = true;

    if(this.gameStateManager.Timer.GetElapsedTime() % (5000 - difficulty) > (4980 - difficulty) 
    && this.midShipSpawn) 
    {
      SpawnMidShips();
      this.midShipSpawn = false;
    }else this.midShipSpawn = true;

    if(this.gameStateManager.Timer.GetElapsedTime() % (10000 - difficulty) > (9980 - difficulty) 
    && this.bigShipSpawn)
    {
      SpawnBigShips();
      this.bigShipSpawn = false;
    }else this.bigShipSpawn = true;

    if (this.gameStateManager.Timer.GetElapsedTime() % 20000 > 19980 
    && this.pickupSpawn)
    {
      SpawnPickup();
      this.pickupSpawn = false;
    }else this.pickupSpawn = true;

  }
  else
  {
    this.notificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Play, ["sound_menu"]));
    this.notificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Pause, ["sound_game"]));
  }
}

function Draw(gameTime) {
  //clear screen on each draw of ALL sprites (i.e. menu and game sprites)
  ClearScreen(Color.Black);

  //draw all the game sprites
  this.objectManager.Draw(gameTime);
  DrawText(gameTime);
}

function DrawText(gameTime)
{
  ctx.save();
  ctx.font = "70px has";
  ctx.fillStyle = "red";
  drawHealth();
  
  ctx.font = "30px ethnocentricregular";
  ctx.fillText("Score: " + this.gameStateManager.score, 10, 30);
  ctx.fillText("Difficulty: " + (Math.round(difficulty/100)), 10, 880);
  ctx.fillText("FPS: " + gameTime.FPS, 700, 30);
  ctx.restore()
}

function drawHealth()
{
  let health = this.gameStateManager.PlayerHealth;
  let healthStr = "";
  for(let i = 0; i < health; i++)
  {
    if(i < 5)
      healthStr += "F";
  }
  ctx.fillText(healthStr, 10, 940);
  ctx.font = "50px ethnocentric";
  if(health > 6)
  {
    ctx.fillText("+" + (health - 6), 670, 940);
  }
}

function ClearScreen(color) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(0, 0, cvs.clientWidth, cvs.clientHeight);
  ctx.restore();
}

// #endregion

/************************************************************ YOUR GAME SPECIFIC UNDER THIS POINT ************************************************************/

// #region  Initialize, Load
function Initialize() {
  InitializeScreenRectangle();
  LoadAssets();
  LoadNotificationCenter();
  LoadManagers();
  LoadSprites();
  this.isPlaying = false;
}

function InitializeScreenRectangle() 
{
  this.screenRectangle = new Rect(
    cvs.clientLeft,
    cvs.clientTop,
    cvs.clientWidth,
    cvs.clientHeight
  );
}

function LoadNotificationCenter() 
{
  this.notificationCenter = new NotificationCenter();
}

function LoadManagers()
 {
  let debugEnabled = false;
  let scrollBoundingBoxBorder = 20;
  this.objectManager = new MyObjectManager(
    "game sprites",
    StatusType.IsUpdated | StatusType.IsDrawn,
    this.notificationCenter,
    this.cvs,
    this.ctx,
    debugEnabled,
    scrollBoundingBoxBorder
  );

  //checks for keyboard input
  this.keyboardManager = new KeyboardManager();
  //adds support for storing and responding to changes in game state e.g. player collect all inventory items, or health == 0
  this.gameStateManager = new MyGameStateManager("store and manage game state", this.notificationCenter);

  //audio - step 3 - instanciate the sound manager with the array of cues
  this.soundManager = new SoundManager(audioCueArray, this.notificationCenter);

  //adds support for a menu system
  this.menuManager = new MyMenuManager(this.notificationCenter, this.keyboardManager);

  //load other managers...
}
function LoadAssets() {
  //textures
  this.spriteSheet = document.getElementById("spacerush_sprite_sheet");
  this.asteroidSpriteSheet = document.getElementById("spacerush_asteroid_sprite_sheet");
  this.backgroundSpriteSheet = document.getElementById("spacerush_background");
}

function LoadSprites() {

  LoadBackground();
  LoadPlayer();
}

function LoadBackground() 
{
  for(let i = 0; i < BACKGROUND_DATA.length; i++)
  {
    let spriteArtist = new ScrollingSpriteArtist(
      ctx,
      BACKGROUND_DATA[i].spriteSheet,
      BACKGROUND_DATA[i].sourcePosition,
      BACKGROUND_DATA[i].sourceDimensions,
      cvs.width,
      cvs.height
    );
    let transform = new Transform2D(
      BACKGROUND_DATA[i].translation,
      BACKGROUND_DATA[i].rotation,
      BACKGROUND_DATA[i].scale,
      BACKGROUND_DATA[i].origin,
      new Vector2(cvs.clientWidth, cvs.clientHeight)
    );
    this.objectManager.Add(
      new Sprite(
        BACKGROUND_DATA[i].id,
        BACKGROUND_DATA[i].actorType,
        transform,
        spriteArtist,
        StatusType.IsUpdated | StatusType.IsDrawn,
        BACKGROUND_DATA[i].scrollSpeedMultiplier,
        BACKGROUND_DATA[i].layerDepth,
      )
    );
  }
  
  //sort all background sprites by depth 0 (back) -> 1 (front)
  this.objectManager.SortAllByDepth(this.objectManager.BackgroundSprites, 
    function sortAscendingDepth(a, b) {return a.LayerDepth - b.LayerDepth});
}

function SpawnAsteroids()
{
  new Asteroid(this.ctx, this.asteroidSpriteSheet, this.objectManager);
}

function SpawnPickup()
{
  let randomPickup = Math.round(Math.random() * (1));
  if(randomPickup == 1)
    new PowerShotPickup(this.ctx, this.spriteSheet, this.objectManager);
  else if(randomPickup == 0)
    new SpreadShotPickup(this.ctx, this.spriteSheet, this.objectManager);
  
  new HealthPickup(this.ctx, this.spriteSheet, this.objectManager);
}

function SpawnBigShips()
{
  new BigShip(this.ctx, this.spriteSheet, this.objectManager, this.screenRectangle);
}

function SpawnMidShips()
{
  new MidShip(this.ctx, this.spriteSheet, this.objectManager, this.screenRectangle);
}

function SpawnSmallShips()
{
  new SmallShip(this.ctx, this.spriteSheet, this.objectManager);
}

function LoadPlayer() {
  new Player(this.ctx, this.spriteSheet, this.objectManager, this.keyboardManager,  this.screenRectangle);
}
// #endregion 
