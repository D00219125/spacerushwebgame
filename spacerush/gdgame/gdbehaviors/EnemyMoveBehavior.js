class EnemyMoveBehavior{

    constructor(objectManager, screenRectangle, heightStop)
    {
        this.objectManager = objectManager;
        this.screenRectangle = screenRectangle;
        this.heightStop = Math.round(Math.random() * heightStop);
        this.goRight = false;
    }

    CheckEnemyBounds(boundingBox, translateBy)
    {
        //Check sides of enemy bounding box + its movement vecotor against screen bounds, return true
        if(boundingBox.X + translateBy.X < this.screenRectangle.X)
        {
            return false;
        }
        if(boundingBox.X + boundingBox.Width + translateBy.X > this.screenRectangle.Width)
        {
            return false;
        }
        return true;
    }

    //#region Core Methods - doesnt need to change
    Execute(gameTime, parent)
    {
        if(parent.Transform2D.Translation.Y > this.heightStop)
        {
           if(this.CheckEnemyBounds(parent.Transform2D.BoundingBox, new Vector2(BIG_SHIP_SIDE_SCROLL_SPEED, 0)) == false)
           {
                this.goRight = !this.goRight;
           }
           if(this.goRight)
            {
                parent.ScrollSpeedMultiplier = new Vector2(BIG_SHIP_SIDE_SCROLL_SPEED, 0);
            }
            else parent.ScrollSpeedMultiplier = new Vector2(-BIG_SHIP_SIDE_SCROLL_SPEED, 0);
        }
    }

    //#endregion

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }


    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}