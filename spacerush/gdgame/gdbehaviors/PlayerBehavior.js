/**
 * Moves the parent sprite based on keyboard input.
 * @author
 * @version 1.0
 * @class PlayerBehavior
 */
class PlayerBehavior
{
    //fields
    keyboardManager;
    moveDirectionHorizontal;
    moveDirectionVertical;
    moveSpeedMultiplier;
    velocity;
    parentSpriteArtist;
    idleCells;
    leftCells;
    rightCells;
    upCells;
    downCells;

    constructor(keyboardManager, objectManager, screenRectangle,
        horizontalVelocity,
        verticalVelocity,
        idleCells, leftCells, rightCells, upCells, downCells,
        fireInterval,
        bulletSprite,
        powerShotSprite,
        spreadShotSprite
    )
    {
        this.keyboardManager = keyboardManager;
        this.objectManager = objectManager;
        this.screenRectangle = screenRectangle;

        this.horizontalVelocity = horizontalVelocity;
        this.verticalVelocity = verticalVelocity;
        this.idleCells = idleCells;
        this.leftCells = leftCells;
        this.rightCells = rightCells;
        this.upCells = upCells;
        this.downCells = downCells;
        this.originalBulletSprite = this.bulletSprite = bulletSprite;
        this.powerShotSprite = powerShotSprite;
        this.spreadShotSprite = spreadShotSprite;
        this.originalFireInterval = this.fireInterval = fireInterval;
        this.timeSinceLastInMs = 0;
        this.pickupShotTimer = new Stopwatch();
        this.pickupShotTimer.Reset();
        this.invulnerabilityTimer = new Stopwatch();
        this.invulnerabilityTimer.Reset();
    }

    //#region Your Game Specific Methods - add code for more CD/CR or input handling 
    HandleInput(gameTime, parent)
    {
        this.HandleMove(gameTime, parent);
        this.HandleShoot(gameTime, parent);
        //your game - add more input handling here...
        //this.HandleMouse(gameTime, parent);
    }

    HandleShoot(gameTime, parent)
    {
        this.timeSinceLastInMs += gameTime.ElapsedTimeInMs;

        //make or clone a bullet
        let bulletClone;
        if(this.keyboardManager.IsKeyDown(Keys.Space) && this.timeSinceLastInMs >= this.fireInterval)
        {
            bulletClone = this.bulletSprite.Clone();
            if(this.bulletSprite instanceof Bullet)
            {
                this.Shoot(parent, bulletClone, BULLET_PLAYER_OFFSET, BULLET_PLAYER_SOUND);
            }
            else if(this.bulletSprite instanceof PowerShot)
            {
                this.Shoot(parent, bulletClone, BULLET_POWERSHOT_OFFSET, BULLET_POWERSHOT_SOUND);
            }
            else if(this.bulletSprite instanceof SpreadShot)
            {
                for(let i = 0; i < 3; i++)
                {
                    bulletClone = this.bulletSprite.Clone();
                    let translateBy = new Vector2(
                        BULLET_PLAYER_OFFSET.X + 80 + (80 * -i),
                        BULLET_PLAYER_OFFSET.Y)
                    this.Shoot(parent, bulletClone, translateBy, BULLET_SPREADSHOT_SOUND);
                }
            }
        }
        
    }

    Shoot(parent, bulletClone, translateBy, sound)
    {
        //set the position of the bullet
        bulletClone.Transform2D.Translation = parent.Transform2D.Translation.Clone();

        bulletClone.Transform2D.TranslateBy(translateBy);

        //enable the bullet
        bulletClone.StatusType = StatusType.IsUpdated | StatusType.IsDrawn;

        //add bullet to object manager
        this.objectManager.Add(bulletClone);
        
        //play fire sound!
        notificationCenter.Notify(new Notification(
            NotificationType.Sound, NotificationAction.Play, [sound]));
        //if we wanted to set sound volume on all audio clips we could publish this type of event...
        //notificationCenter.Notify(new Notification(NotificationType.Sound, NotificationAction.SetVolumeAll,  [1]));
        
        //reset
        this.timeSinceLastInMs = 0;
    }

    HandleMove(gameTime, parent)
    {
        //player idle
        if(this.keyboardManager.AreKeysDown([Keys.A, Keys.W, Keys.D, Keys.S]) == false)
        {
            parent.Artist.Cells = this.idleCells;
        }

        //go up
        if (this.keyboardManager.IsKeyDown(Keys.W))
        {
            let translateBy = new Vector2(0, -this.verticalVelocity);
            if(this.PlayerBoundsCheckY(parent.Transform2D.BoundingBox, translateBy))
            {
                parent.Transform2D.TranslateBy(translateBy);
                parent.Artist.Cells = this.upCells;
            }
        }
        //go left
        if (this.keyboardManager.IsKeyDown(Keys.A))
        {
            //parent.Body.AddVelocityX(-this.horizontalVelocity * gameTime.ElapsedTimeInMs);]
            let translateBy = new Vector2(-this.horizontalVelocity, 0);
            if(this.PlayerBoundsCheckX(parent.Transform2D.BoundingBox, translateBy))
            {
                parent.Transform2D.TranslateBy(translateBy);
                parent.Artist.Cells = this.leftCells;
            }
        }
        //go down
        if (this.keyboardManager.IsKeyDown(Keys.S))
        {
            let translateBy = new Vector2(0, this.verticalVelocity);
            if(this.PlayerBoundsCheckHeight(parent.Transform2D.BoundingBox, translateBy))
            {
                parent.Transform2D.TranslateBy(translateBy);
                parent.Artist.Cells = this.downCells;
            }
        }
        //go right
        if (this.keyboardManager.IsKeyDown(Keys.D))
        {
            let translateBy = new Vector2(this.horizontalVelocity, 0);
            if(this.PlayerBoundsCheckWidth(parent.Transform2D.BoundingBox, translateBy))
            {
                parent.Transform2D.TranslateBy(translateBy);
                parent.Artist.Cells = this.rightCells;
            }
        }
    }

    PlayerBoundsCheckX(boundingBox, translateBy)
    {
        if(boundingBox.X + translateBy.X < this.screenRectangle.X)
        {
            return false;
        }
        return true;
    }
    PlayerBoundsCheckWidth(boundingBox, translateBy)
    {
        if(boundingBox.X + boundingBox.Width + translateBy.X > this.screenRectangle.Width)
        {
            return false;
        }
        return true;
    }
    PlayerBoundsCheckY(boundingBox, translateBy)
    {
        if(boundingBox.Y + translateBy.Y < this.screenRectangle.Y)
        {
            return false;
        }
        return true;
    }
    PlayerBoundsCheckHeight(boundingBox, translateBy)
    {
        if(boundingBox.Y + boundingBox.Height + translateBy.Y > this.screenRectangle.Height)
        {
            return false;
        }
        return true;
    }
        

    CheckCollisions(gameTime, parent)
    {
        //your game - add methods for each array type in MyObjectManager that we can collide with
        this.HandleEnemyCollision(parent);
        this.HandlePickupCollision(gameTime, parent);
        this.HandleAllCollisionWithBullet();
    }

    HandlePickupCollision(gameTime, parent)
    {
        for (let i = 0; i < this.objectManager.InteractableSprites.length; i++) {

            let sprite = this.objectManager.InteractableSprites[i];

            //we can use simple collision check here (i.e. Intersects) because dont need to think was it top, bottom, left, or right
            if(parent.Transform2D.BoundingBox.Intersects(sprite.Transform2D.BoundingBox))
            {
                if(sprite instanceof PowerShotPickup)
                {
                    this.bulletSprite = this.powerShotSprite;
                    this.fireInterval = BULLET_POWERSHOT_INTERVAL_IN_MS;
                }
                else if(sprite instanceof SpreadShotPickup)
                {
                    this.bulletSprite = this.spreadShotSprite;
                    this.fireInterval = BULLET_SPREADSHOT_INTERVAL_IN_MS;
                }
                else if(sprite instanceof HealthPickup)
                {
                    notificationCenter.Notify(new Notification(NotificationType.GameState,
                        NotificationAction.Health, [parent, +2]));
                }

                this.pickupShotTimer.Reset();
                this.pickupShotTimer.Start();

                notificationCenter.Notify(new Notification(NotificationType.Sprite, 
                    NotificationAction.Remove, [sprite]));
                
                notificationCenter.Notify(new Notification(
                    NotificationType.Sound, NotificationAction.Play, ["sound_pickup"]));
            }


        }

    }

    HandleEnemyCollision(parent)
    {
        if(this.invulnerabilityTimer.GetElapsedTime() == 0)
        {
            for (let i = 0; i < this.objectManager.EnemySprites.length; i++)
            {

                let sprite = this.objectManager.EnemySprites[i];
                
                if(parent.Transform2D.BoundingBox.Intersects(sprite.Transform2D.BoundingBox))
                {
                    this.invulnerabilityTimer.Reset();
                    this.invulnerabilityTimer.Start();
                    //remove enemy sprite that was crashed into
                    notificationCenter.Notify(new Notification(NotificationType.Sprite, 
                        NotificationAction.Remove, [sprite]));
                    
                    //remove health
                    notificationCenter.Notify(new Notification(NotificationType.GameState, 
                        NotificationAction.Health, [parent, -1]));

                    //notificationCenter.Notify(new Notification(NotificationType.Sprite, 
                    //    NotificationAction.Spawn,  [new Vector2(RUNNER_START_X_POSITION, RUNNER_START_Y_POSITION)]));

                    /*
                    //audio - step 4 - create a notification and request one of the unique IDs from the cues
                    notificationCenter.Notify(new Notification(NotificationType.Sound, 
                        NotificationAction.Play,  ["damage_player"]));
                    */
                }

            }
        }
    }

    HandleAllCollisionWithBullet()
    {
        for(let i = 0; i < this.objectManager.BulletSprites.length; i++)
        {
            let bullet = this.objectManager.BulletSprites[i];
            if(bullet instanceof EnemyBullet && this.invulnerabilityTimer.GetElapsedTime() == 0)
            {
                for(let j = 0; j < this.objectManager.PlayerSprites.length; j++)
                {
                    let player = this.objectManager.PlayerSprites[j];
                    if(bullet.Transform2D.BoundingBox.Intersects(player.Transform2D.BoundingBox))
                    {
                        let position = new Vector2(
                            bullet.Transform2D.Translation.X + (bullet.Transform2D.BoundingBox.Width/2),
                            bullet.Transform2D.Translation.Y
                        );
                        new Explosion(ctx, spriteSheet, objectManager, position);

                        this.invulnerabilityTimer.Reset();
                        this.invulnerabilityTimer.Start();

                        //remove bullet
                        notificationCenter.Notify(new Notification(NotificationType.Sprite, 
                            NotificationAction.Remove, [bullet]));

                        //subtract health
                        notificationCenter.Notify(new Notification(NotificationType.GameState, 
                            NotificationAction.Health, [player, -1]));

                        
                    }
                }
            }
            else if(!(bullet instanceof EnemyBullet))
            {  
                for(let j = 0; j < this.objectManager.EnemySprites.length; j++)
                {
                    let enemy = this.objectManager.EnemySprites[j];
                    if(bullet.Transform2D.BoundingBox.Intersects(enemy.Transform2D.BoundingBox))
                    {
                        let position = new Vector2(
                            bullet.Transform2D.Translation.X + (bullet.Transform2D.BoundingBox.Width/2),
                            bullet.Transform2D.Translation.Y
                        );
                        new Explosion(ctx, spriteSheet, objectManager, position);

                        //remove bullet
                        notificationCenter.Notify(new Notification(NotificationType.Sprite, 
                            NotificationAction.Remove, [bullet]));

                        //sound
                        notificationCenter.Notify(new Notification(
                            NotificationType.Sound, NotificationAction.Play, ["sound_ship_hit"]));

                        if(bullet instanceof Bullet || bullet instanceof SpreadShot)
                        {
                            //subtract health
                            notificationCenter.Notify(new Notification(NotificationType.GameState, 
                                NotificationAction.Health, [enemy, -1]));
                        }
                        else if(bullet instanceof PowerShot)
                        {
                            notificationCenter.Notify(new Notification(NotificationType.GameState, 
                                NotificationAction.Health, [enemy, -2]));
                        }
                    }
                }
            }
        }
    }

    //#endregion

    //#region Core Methods - doesnt need to change
    Execute(gameTime, parent)
    {
        this.HandleInput(gameTime, parent);
        this.CheckCollisions(gameTime, parent);
        if(this.pickupShotTimer.GetElapsedTime() >= 10000)
        {
            this.pickupShotTimer.Reset();
            this.bulletSprite = this.originalBulletSprite;
            this.fireInterval = this.originalFireInterval;
        }
        if(this.invulnerabilityTimer.GetElapsedTime() >= 1)
        {
            this.invulnerabilityTimer.Reset();
        }
        if(this.invulnerabilityTimer.GetElapsedTime() != 0 && parent.StatusType != StatusType.IsUpdated)
        {
            parent.statusType = StatusType.IsUpdated;
        }
        else
        {
            parent.statusType = StatusType.IsUpdated | StatusType.IsDrawn;
        }
    }

    //#endregion

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }


    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}