/**
 * Stores, updates, and draws all sprites in my specific game e.g. Snailbait.
 * @author
 * @version 1.0
 * @class MyObjectManager
 */

class MyObjectManager extends ObjectManager {

    //PC and NPCs
    enemySprites = [];
    playerSprites = [];
    bulletSprites = [];

    //stores all the background sprites e.g. trees, clouds, mountains
    backgroundSprites = [];

    //stores all interactable sprites e.g. ammo, pickup
    interactableSprites = [];

    //stores all other sprites
    decoratorSprites = [];

    //this offset will be applied to all sprites listed in UpdateGlobalTranslationOffset()     
    deltaTranslationOffset = Vector2.Zero;
    translationOffset = Vector2.Zero;

    //these getters allow a collision manager to get access to the sprites for collision detection/collision response (CD/CR) testing
    get EnemySprites() { return this.enemySprites;}
    get PlayerSprites() { return this.playerSprites;}
    get BulletSprites() { return this.bulletSprites;}
    get InteractableSprites() { return this.interactableSprites;}
    get BackgroundSprites() { return this.backgroundSprites;}

    get SmallShipCount() { return this.smallShipCount;}
    get MidShipCount() { return this.midShipCount;}
    get BigShipCount() { return this.bigShipCount;}
    get AsteroidCount() { return this.asteroidCount;}
    get BulletCount() { return this.bulletCount;}
    get PowerShotCount() { return this.powerShotCount;}
    get SpreadShotCount() { return this.spreadShotCount;}
    get PickupCount() {return this.pickupCount; }

    /**
     * Used by a player sprite to set the translation offset for all sprites that need to be moved when player moves.
     * e.g. in a scrollable game we move everything around the player instead of moving the player.     *
     * @memberof MyObjectManager
     */
    set DeltaTranslationOffset(deltaTranslationOffset) {
        this.deltaTranslationOffset = deltaTranslationOffset;
        this.translationOffset.Add(deltaTranslationOffset);
        this.IsDirty = true;
    }

    constructor(
        id,
        statusType,
        notificationCenter,
        canvas,
        context,
        debugEnabled,
        scrollBoundingBoxBorder
      ) {
        super(id, statusType, canvas, context, debugEnabled, scrollBoundingBoxBorder);
        this.notificationCenter = notificationCenter;
        this.RegisterForNotifications();
        this.bigShipCount = 0;
        this.midShipCount = 0;
        this.smallShipCount = 0;
        this.asteroidCount = 0;
        this.bulletCount = 0;
        this.powerShotCount = 0;
        this.spreadShotCount = 0;
        this.pickupCount = 0;
    }

    //handle all GameState type events - see PlayerBehavior::HandleEnemyCollision()
    RegisterForNotifications()
    {
        //handle events related to add/remove sprites
        this.notificationCenter.Register(
          NotificationType.Sprite,
          this,
          this.HandleSpriteNotification
        );
    
        this.notificationCenter.Register(
          NotificationType.Menu,
          this,
          this.HandleNotification
        );
    }

    HandleSpriteNotification(...argArray)
    {
        let notification = argArray[0];
        switch(notification.NotificationAction)
        {
            case NotificationAction.Remove:
                this.HandleRemoveSprite(notification.NotificationArguments);  
                break
            default:
                break;  
            //add more cases for volume changes etc
        }
    }

    HandleNotification(...argArray)
    {
        let notification = argArray[0];
        switch(notification.NotificationAction)
        {
            case NotificationAction.ShowMenuChanged:
                this.HandleShowMenu(notification.NotificationArguments);
                break;
            
            default:
                break;  
            //add more cases for volume changes etc
        }
    }

    /**
     * Call this method before the game starts and after we add
     * all the sprites to sort the draw order of the sprites by depth
     * where 0 == back and 1 == front.
     *
     * @memberof MyObjectManager
     */
    SortAllByDepth(spriteArray, sortFunc)
    {
        //sort functions need to return -ve, 0, or +ve number
        spriteArray.sort(sortFunc);
    }

    Add(sprite) {
        //is it valid and the correct object type?
        if (sprite != null && sprite instanceof Sprite) {
            //does it have a sprite type?
            if (sprite.ActorType) {
                switch (sprite.ActorType) {
                    case ActorType.Enemy:
                        this.enemySprites.push(sprite);
                        if(sprite instanceof Asteroid)
                        {
                            this.asteroidCount++;
                            this.NotifySpawnWithHealth(sprite, ASTEROID_HEALTH);
                        }
                        else if(sprite instanceof BigShip)
                        {
                            this.bigShipCount++;
                            this.NotifySpawnWithHealth(sprite, BIG_SHIP_HEALTH);
                        }
                        else if(sprite instanceof MidShip)
                        {
                            this.midShipCount++;
                            this.NotifySpawnWithHealth(sprite, MID_SHIP_HEALTH);
                        }
                        else if(sprite instanceof SmallShip)
                        {
                            this.smallShipCount++;
                            this.NotifySpawnWithHealth(sprite, SMALL_SHIP_HEALTH);
                        }
                        break;
                    case ActorType.Player:
                        this.playerSprites.push(sprite);
                        this.NotifySpawnWithHealth(sprite, PLAYERSHIP_HEALTH);
                        break;
                    case ActorType.Bullet:
                        if(sprite instanceof Bullet)
                            this.bulletCount++;
                        else if(sprite instanceof PowerShot)
                            this.powerShotCount++;
                        else if(sprite instanceof SpreadShot)
                            this.spreadShotCount++;
                            this.bulletSprites.push(sprite);
                        break;
                    case ActorType.Background:
                        this.backgroundSprites.push(sprite);
                        break;
                    //notice we can group lots of similar actor types into a single array i.e. interactables    
                    case ActorType.Health:
                    case ActorType.Inventory:
                    case ActorType.Interactable:
                        if(sprite instanceof PowerShotPickup)
                        {
                            this.pickupCount++;
                            this.interactableSprites.push(sprite);
                            break;
                        }
                        if (sprite instanceof SpreadShotPickup)
                        {
                            this.pickupCount++;
                            this.interactableSprites.push(sprite);
                            break;
                        }
                    case ActorType.Ammo:                     
                        this.interactableSprites.push(sprite);
                        break;

                    //add more cases for each of the new ActorTypes that you add in your game
                    default:
                        this.decoratorSprites.push(sprite);
                        break;
                }
            }
        }
    }

    HandleShowMenu(argArray) 
    {
        this.statusType = argArray[0];
    }

    NotifySpawnWithHealth(sprite, health)
    {
        this.notificationCenter.Notify(new Notification(NotificationType.GameState, 
            NotificationAction.Spawn, [sprite, health]));
    }

    NotifyDespawn(sprite)
    {
        this.notificationCenter.Notify(new Notification(NotificationType.GameState, 
            NotificationAction.Despawn, [sprite]));
    }

    HandleRemoveSprite(argArray)
    {
        let argID = argArray[0].ID;
        
        switch(argArray[0].ActorType)
        {
            case ActorType.Enemy:
                for(let i = 0; i < this.enemySprites.length; i++)
                {
                    if(this.enemySprites[i].ID == argID)
                    {
                        this.enemySprites.splice(i, 1);
                        return;
                    }
                }
                break;
            case ActorType.Player:
                for(let i = 0; i < this.playerSprites.length; i++)
                {
                    if(this.playerSprites[i].ID == argID)
                    {
                        console.log("You Lose!");
                        this.playerSprites.splice(i, 1);
                        this.ResetAll();
                        return;
                    }
                }
                break;
            case ActorType.Bullet:
                for(let i = 0; i < this.bulletSprites.length; i++)
                {
                    if(this.bulletSprites[i] == argArray[0])
                    {
                        this.bulletSprites.splice(i, 1);
                        return;
                    }
                }
                break;
            case ActorType.Interactable:
                for(let i = 0; i < this.interactableSprites.length; i++)
                {
                    if(this.interactableSprites[i].ID == argID)
                    {
                        this.interactableSprites.splice(i, 1);
                        return;
                    }
                }
                break;
            case ActorType.decoratorSprites:
                for(let i = 0; i < this.decoratorSprites.length; i++)
                {
                    if(this.decoratorSprites[i].ID == argID)
                    {
                        this.decoratorSprites.splice(i, 1);
                        return;
                    }
                }
                break;                    
            default:
                break;
        }
    }

    ResetAll()
    {
        this.notificationCenter.Notify(new Notification(
            NotificationType.Time, NotificationAction.Reset));
          this.notificationCenter.Notify(new Notification(
            NotificationType.Time, NotificationAction.Pause));
          this.notificationCenter.Notify(new Notification(
            NotificationType.Menu, NotificationAction.ShowMenuChanged,  [StatusType.Off | StatusType.IsDrawn]));
    }

//#region Optional Functions
    Get(spriteID) {
        //to do....      
    }

    Get(spriteID, actorType) {
        //to do....      
    }

    Remove(spriteID) {
        //to do....
    }

    Remove(spriteID, actorType) {
        //to do....
    }

    RemoveAll(actorType) {
        //to do....
    }

    Clear() {
        //to do....
    }
//#endregion

Update(gameTime) {
    //should we be updating? if menu is shown then we should not
    if ((this.statusType & StatusType.IsUpdated) != 0) {
      this.UpdateTranslationOffset(gameTime);
      this.UpdateAll(gameTime);
    }
  }

  Draw(gameTime) {
    //should we be drawing? if menu is shown then we should not
    if ((this.statusType & StatusType.IsDrawn) != 0) {
      this.DrawAll(gameTime);
      if (this.DebugEnabled)
        this.DrawDebug("red", "green", "white", "yellow");
    }
  }
    /**
     * Call this to re-position all the sprites that move around the player sprite i.e. to enable side-scrolling.
     * Note: Player sprite is not updated here since all sprites move around the player sprite so its translation offset is zero.
     * @param {GameTime} gameTime
     * @memberof MyObjectManager
     */
    UpdateTranslationOffset(gameTime)
    {
        //add to each sprites existing translation offset if non-zero
        if(this.deltaTranslationOffset.Length() != 0)
        {
            this.deltaTranslationOffset.MultiplyScalar(gameTime.ElapsedTimeInMs);

            //for (let i = 0; i < this.enemySprites.length; i++)
            //this.enemySprites[i].Transform2D.SetTranslationOffset(this.translationOffset);

            for (let i = 0; i < this.enemySprites.length; i++)
                this.enemySprites[i].Transform2D.TranslateBy(
                    this.enemySprites[i].ScrollSpeedMultiplier
                );
            // for (let i = 0; i < this.bulletSprites.length; i++)
            //     this.bulletSprites[i].Transform2D.AddToTranslationOffset(
            //         this.deltaTranslationOffset);

            for (let i = 0; i < this.decoratorSprites.length; i++)
                this.decoratorSprites[i].Transform2D.TranslateBy(
                    this.decoratorSprites[i].ScrollSpeedMultiplier
                );

            for (let i = 0; i < this.interactableSprites.length; i++)
                this.interactableSprites[i].Transform2D.TranslateBy(
                    this.interactableSprites[i].ScrollSpeedMultiplier
                );

            for (let i = 0; i < this.backgroundSprites.length; i++)
                this.backgroundSprites[i].Transform2D.AddToTranslationOffset(
                    this.deltaTranslationOffset
                );

            //set the delta back to zero, otherwise it will keep being applied in each Update()
            this.deltaTranslationOffset = Vector2.Zero;   
        }
    }

    UpdateAll(gameTime){

        for (let i = 0; i < this.enemySprites.length; i++)
            this.enemySprites[i].Update(gameTime);

        for (let i = 0; i < this.playerSprites.length; i++)
            this.playerSprites[i].Update(gameTime);

        for (let i = 0; i < this.bulletSprites.length; i++)
            this.bulletSprites[i].Update(gameTime);

        for (let i = 0; i < this.interactableSprites.length; i++)
            this.interactableSprites[i].Update(gameTime);

        for (let i = 0; i < this.decoratorSprites.length; i++)
            this.decoratorSprites[i].Update(gameTime);

        for (let i = 0; i < this.backgroundSprites.length; i++)
            this.backgroundSprites[i].Update(gameTime);
    }


    /**
     * Draw all game sprites. Note, the order in which we draw each array is important. We MUST always draw
     * from the back of the screen forward i.e. call for(backgroundSprites)... to for(playerSprites).
     *
     * @param {GameTime} gameTime
     * @memberof MyObjectManager
     */
    DrawAll(gameTime){
        
        this.drawn = 0;

        for (let i = 0; i < this.backgroundSprites.length; i++)
            this.backgroundSprites[i].Draw(gameTime);

                    
        //this will cull(remove) sprites that arent visible on the screen
        for (let i = 0; i < this.decoratorSprites.length; i++)
        {
            if(this.screenBoundingBox.Intersects(this.decoratorSprites[i].Transform2D.BoundingBox))
                this.decoratorSprites[i].Draw(gameTime);
                if(this.decoratorSprites[i] instanceof Explosion)
                {
                    //If at end of explosion animation, remove
                    if(this.decoratorSprites[i].Artist.CurrentCellIndex == EXPLOSION_CELLS.length - 1)
                    {
                        this.decoratorSprites.splice(i, 1);
                    }
            }
            else this.decoratorSprites.splice(i, 1);
        }

        //this will cull(remove) sprites that arent visible on the screen
        for (let i = 0; i < this.interactableSprites.length; i++)
        {
            if(this.screenBoundingBox.Intersects(this.interactableSprites[i].Transform2D.BoundingBox))
                this.interactableSprites[i].Draw(gameTime);
            else this.interactableSprites.splice(i, 1); 
        }

        //this will cull(remove) sprites that arent visible on the screen
        for (let i = 0; i < this.enemySprites.length; i++)
        {
            if(this.screenBoundingBox.Intersects(this.enemySprites[i].Transform2D.BoundingBox))
                this.enemySprites[i].Draw(gameTime);
            else 
            {
                this.NotifyDespawn(this.enemySprites[i]);
                this.enemySprites.splice(i, 1);
            }
        }

        for (let i = 0; i < this.bulletSprites.length; i++)
        {
            if(this.screenBoundingBox.Intersects(this.bulletSprites[i].Transform2D.BoundingBox))
                this.bulletSprites[i].Draw(gameTime);
            else this.bulletSprites.splice(i, 1);
        }

            for (let i = 0; i < this.playerSprites.length; i++)
            this.playerSprites[i].Draw(gameTime);
    }

//#region Debug
    //draws the CD/CR Rect around sprites if enabled
    DrawDebug(debugEnemyColor, debugInteractableColor, debugPlayerColor, debugBulletColor){

        for (let i = 0; i < this.enemySprites.length; i++)
            this.DrawDebugBoundingBox(debugEnemyColor, this.enemySprites[i]);
    
        for (let i = 0; i < this.interactableSprites.length; i++)
            this.DrawDebugBoundingBox(debugInteractableColor, this.interactableSprites[i]);
    
        for (let i = 0; i < this.playerSprites.length; i++)
            this.DrawDebugBoundingBox(debugPlayerColor, this.playerSprites[i]);
    
        for (let i = 0; i < this.bulletSprites.length; i++)
            this.DrawDebugBoundingBox(debugBulletColor, this.bulletSprites[i]); 
            
        //add more for loops here for any other arrays containing sprites with bounding boxes...
          
        }
    //#endregion
    
    }